" Vim plugin for editing Ableton Live sets.
" Maintainer: Peter Morris <podmorris@mac.com>
" Last Change: 2018 Jul 30

" Exit quickly when:
" - this plugin was already loaded
" - when 'compatible' is set
" - some autocommands are already taking care of compressed files
if exists("loaded_ableton") || &cp || exists("#BufReadPre#*.als")
  finish
endif
let loaded_ableton = 1

augroup ableton
  " Remove all gzip autocommands
  au!

  " Enable editing of gzipped files.
  " The functions are defined in autoload/gzip.vim.
  "
  " Set binary mode before reading the file.
  " Use "gzip -d", gunzip isn't always available.
  autocmd BufReadPost,FileReadPost	*.als  call gzip#read("gzip -dn --suffix .als")
  autocmd BufWritePost,FileWritePost	*.als  call gzip#write("gzip --suffix .als")
  autocmd FileAppendPre			*.als  call gzip#appre("gzip -dn --suffix .als")
  autocmd FileAppendPost		*.als  call gzip#write("gzip --suffix .als")
augroup END
