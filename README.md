# ableton.vim

Simple plugin to enable editing of Ableton Live Sets `.als` files with `vim`.
